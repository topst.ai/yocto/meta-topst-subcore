DESCRIPTION = "This image provides cortex-a53 subcore"
LICENSE = "MIT"

inherit core-image

IMAGE_FSTYPES = "ext4"
IMAGE_ROOTFS_SIZE = "10240"
IMAGE_OVERHEAD_FACTOR = "1.2"
IMAGE_ROOTFS_EXTRA_SPACE ?= "10240"
IMAGE_ROOTFS_ALIGNMENT = "1024"

IMAGE_INSTALL = " \
    packagegroup-topst-subcore-base \
	kernel-modules \
	kmod \
	hsm \
    ${CORE_IMAGE_EXTRA_INSTALL} \
"
